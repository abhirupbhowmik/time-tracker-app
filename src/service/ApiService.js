import axios from 'axios';

const TIME_TRACKER_PATH = '/records';

class ApiService {


    async fetchAllRecords() {
        return axios.get(TIME_TRACKER_PATH);
    }

    async fetchlatestRecord() {
        return axios.get(TIME_TRACKER_PATH + '?offset=0&length=10');
    }

    async fetchRecordByEmailId(emailId) {
        return axios.get(TIME_TRACKER_PATH + '?email=' + emailId);
    }

     async addRecord(record) {
        return axios({
            method: 'post',
            url: TIME_TRACKER_PATH,
            data: record,
            headers: {'Content-Type': 'multipart/form-data' }
            })
            .then(function (response) {
                //handle success
                console.log('SUCCESS : ' + response);
            })
            .catch(error => {
                console.log('ERROR : ' + error);
            });
    }

}

export default new ApiService();