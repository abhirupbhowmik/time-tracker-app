import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import React from "react";
import AddRecordComponent from './record/AddRecordComponent';
import ListRecordComponent from "./record/ListRecordComponent";

const AppRouter = () => {
    return(
        <div style={style}>
            <Router>
                    <Switch>
                        <Route path="/" exact component={ListRecordComponent} />
                        <Route path="/records" component={ListRecordComponent} />
                        <Route path="/add-record" component={AddRecordComponent} />
                    </Switch>
            </Router>
        </div>
    )
}

const style={
    marginTop:'20px'
}

export default AppRouter;