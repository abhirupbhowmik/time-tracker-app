import React, { Component } from 'react'
import ApiService from "../../service/ApiService";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import TableContainer from '@material-ui/core/TableContainer';
import Paper from '@material-ui/core/Paper';

class ListRecordComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            searchBox: '',
            records: [],
        }
        this.addRecord = this.addRecord.bind(this);
        this.reloadLastTenRecordList = this.reloadLastTenRecordList.bind(this);
    }

    componentDidMount() {
        this.reloadLastTenRecordList();
    }

    reloadLastTenRecordList() {
        ApiService.fetchlatestRecord()
            .then((res) => {
                this.setState({ records: res.data })
            });
        ApiService.fetchAllRecords()
            .then((res) => {
                alert('All records has been loaded');
                this.setState({ records: res.data })
            });
    }

    addRecord() {
        this.props.history.push('/add-record');
    }


    searchWithEmailAddress() {
        let emailId = this.state.searchBox;
        ApiService.fetchRecordByEmailId(emailId)
            .then((res) => {
                if (res.data == '') {
                    alert('Time Record for employee ' + emailId + ' not found')
                    this.props.history.push('/');
                    this.setState({ searchBox: '' });
                } else {
                    this.props.history.push('/');
                    this.setState({ records: res.data });
                    this.setState({ searchBox: '' });
                }
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return (
            <TableContainer component={Paper}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell align="left">
                                <Button variant="contained" color="secondary" onClick={() => this.addRecord()}>
                                    Add Record
                            </Button>
                            </TableCell>
                            <TableCell></TableCell>
                            <TableCell></TableCell>
                            <TableCell align="right">
                                <TextField type="text" placeholder="Search with Employee Email Id" name="searchBox" fullWidth margin="normal" value={this.state.searchBox} onChange={this.onChange} />
                            </TableCell>
                            <TableCell>
                                <Button variant="contained" color="primary" onClick={() => this.searchWithEmailAddress()}>
                                    Search
                            </Button>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>No</TableCell>
                            <TableCell align="center">Email Id</TableCell>
                            <TableCell align="center">Start Time</TableCell>
                            <TableCell align="center">End Time</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.records.map((row, id) => (
                            <TableRow key={id + 1}>
                                <TableCell component="th" scope="row">
                                    {id + 1}
                                </TableCell>
                                <TableCell align="center">{row.email}</TableCell>
                                <TableCell align="center">{row.start}</TableCell>
                                <TableCell align="center">{row.end}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        );
    }

}

export default ListRecordComponent;