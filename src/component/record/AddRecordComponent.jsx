import React, { Component } from 'react'
import ApiService from "../../service/ApiService";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

class AddRecordComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            startTime: '',
            endTime: '',
            emailAddress: '',
        }
        this.saveRecord = this.saveRecord.bind(this);
    }

    validateEmail(email) {
        var hasErrors = false;
        if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
            alert('Please enter a valid email address');
            hasErrors = true;
        }
        return !hasErrors;
    }

    saveRecord = (e) => {
        e.preventDefault();
        if (!this.validateEmail(this.state.emailAddress)) {
            return;
        }
        var record = new FormData();
        record.set('email', this.state.emailAddress);
        record.set('start', this.state.startTime);
        record.set('end', this.state.endTime);

        ApiService.addRecord(record)
            .then(res => {
                alert('Employee Time Record added successfully' )
                this.props.history.push('/records');
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return (
            <div>
                <Typography variant="h4" color="primary" style={style}>Add Employee Record</Typography>
                <form style={formContainer}>
                    <TextField type="text" placeholder="Please enter your start time in DD.MM.YYYY HH:MM format" fullWidth margin="normal" name="startTime" value={this.state.startTime} onChange={this.onChange} />

                    <TextField type="text" placeholder="Please enter your end time in DD.MM.YYYY HH:MM format" fullWidth margin="normal" name="endTime" value={this.state.endTime} onChange={this.onChange} />

                    <TextField type="email" placeholder="Please enter your email address" fullWidth margin="normal" name="emailAddress" value={this.state.emailAddress} onChange={this.onChange} />

                    <Button variant="contained" color="secondary" onClick={this.saveRecord}>Save Record</Button>
                </form>
            </div>
        );
    }
}
const formContainer = {
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'center',
};

const style = {
    display: 'flex',
    justifyContent: 'center'

}

export default AddRecordComponent;