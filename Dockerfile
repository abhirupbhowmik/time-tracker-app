# Downloads the minimal image of node from Dockerhub
FROM node:alpine

# Create app directory and use it as working directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Copies package.json and package-lock.json from local directory to the working directory inside the docker container
COPY package*.json ./

# Downloads dependencies defined in the package. json file
RUN npm install

# Copy app source code. The first . implies copy source code from current local directory.
# The second . implies that copied source code should be put inside the docker container at the working directory mentioned above
COPY . .

# Builds the application for production to the `build` folder.
#It correctly bundles React in production mode and optimizes the build for the best performance.
#The build is minified and the filenames include the hashes.
RUN npm run build

# Since the frontend will be running on port 3000 inside the container, 
# EXPOSE will allow it to interact with applications outside the container.
EXPOSE 3000

# Once everything is done CMD helps to passes the command separated with commas 
# to be run inside the container to start the frontend application
CMD ["npm", "start"]