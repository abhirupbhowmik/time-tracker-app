## Employee Time Tracker App
#### Version 1.0.1

## Tech Stack
- React JS Framework 16.13
- React Router 5.1.2
- Material-UI 4.9 [React components that implement Google's Material Design]
- Axios [Promise based HTTP client] 
- Node JS 13.3
- NPM 6.14
- Git
- Docker
- VS Code

## Overview
Employee Time Tracker App is a frontend built in React JS framework that keeps track of employee work times.
The frontend is built using Material-UI which features React components implementing Google's Material Design.
In a nut-shell it follows responsive web design which is a web development approach that creates dynamic changes to the appearance of a website, 
depending on the screen size and orientation of the device being used to view it, thus looking good in desktops, tablets and mobile devices.<br />
### Source Code
The source code is availble at GitLab in the below link. Please clone it to get started.
 - https://gitlab.com/abhirupbhowmik/time-tracker-app 
 
Employee Time Tracker App can be deployed as a docker container whose docker image is avaialble at Docker Hub at the following URL:
 - https://hub.docker.com/r/abhirupbhowmik/time-tracker-ui
 
The frontend communicates with TimeTracker which is a legacy backend application. <br />
The backend application can be deployed as a docker container, whose docker image is available at the following URL:
 - https://hub.docker.com/r/alirizasaral/timetracker/

### Manual showing how to create docker image from the source code
 Refer to [CreateDockerImageFromSourceCode](CreateDockerImageFromSourceCode.pdf) <br /> <br />
 
###  Manual showing how to deploy and start the frontend
 Refer to [DeployAndStartTheFrontend](DeployAndStartTheFrontend.pdf)





